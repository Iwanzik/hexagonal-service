package de.tarent.ciwanzik.shoppingCart.domain

import spock.lang.Specification

@SuppressWarnings("GroovyResultOfObjectAllocationIgnored")
class SkuTest extends Specification {

    def "A sku must not be empty"() {
        when: "An empty sku"
        new SKU("")

        then:
        thrown(IllegalArgumentException)
    }

    def "A sku must not exceed 20 characters"() {
        when: "A long sku"
        new SKU("123456789013245678901")

        then:
        thrown(IllegalArgumentException)
    }

    def "A sku must not contain no alphanumeric characters"() {
        when: "A sku with no alphanumeric characters"
        new SKU("13246<7890")

        then:
        thrown(IllegalArgumentException)
    }

    def "A SKU can contain up to 20 alphanumeric characters"() {
        when: "A sku with 20 characters"
        new SKU("12345678901234567890")

        then:
        noExceptionThrown()
    }

    def "A SKU can contain 1 character"() {
        when: "A sku with 1 character"
        new SKU("1")

        then:
        noExceptionThrown()
    }
}
