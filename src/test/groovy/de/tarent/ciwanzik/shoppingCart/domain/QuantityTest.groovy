package de.tarent.ciwanzik.shoppingCart.domain


import spock.lang.Specification

@SuppressWarnings("GroovyResultOfObjectAllocationIgnored")
class QuantityTest extends Specification {

    def "A quantity can be zero"() {
        when: "a quantity of 0"
        new Quantity(0)

        then:
        noExceptionThrown()
    }

    def "A quantity can be up to 10"() {
        when: "a quantity of 10"
        new Quantity(10)

        then:
        noExceptionThrown()
    }

    def "A quantity can not be negative"() {
        when: "a quantity of -1"
        new Quantity(-1)

        then:
        thrown(IllegalArgumentException)
    }

    def "A quantity can not exceed 10"() {
        when: "a quantity of 11"
        new Quantity(11)

        then:
        thrown(TooMuchItemsOfAProduct)
    }

}
