package de.tarent.ciwanzik.shoppingCart.domain

import kotlin.Pair
import spock.lang.Specification
import spock.lang.Subject

class ShoppingCartTest extends Specification {

    @Subject
    ShoppingCart cart

    private def sku = new SKU("12345")
    private def secondSku = new SKU("54321")

    def setup() {
        cart = new ShoppingCart()
    }

    def "A Shopping cart can be initialized with no products"() {
        expect: "The cart is empty"
        cart.isEmpty()
    }

    def "A Shopping cart can be initialized with a predefined product set"() {
        given: "A product map"
        Map<Product, Quantity> products = [
            (aProduct(new Price(10, 0))): new Quantity(2),
            (anotherProduct( new Price(2, 0))): new Quantity(4)
        ]

        and: "A shopping cart uuid"
        def uuid = new ShoppingCartUuid()

        when: "An existing shopping cart is initialized"
        def cart = new ShoppingCart(uuid, products)

        then: "The cart has all information"
        cart.shoppingCartUuid == uuid
        cart.amount() == new ShoppingCartAmount(28, 0)

        and: "All products"
        cart.quantityOfProduct(sku).get() == new Quantity(2)
        cart.quantityOfProduct(secondSku).get() == new Quantity(4)
    }

    def "A new shopping cart has an UUID"() {
        expect: "The cart has an uuid"
        cart.getShoppingCartUuid().uuid != null
    }

    def "A shopping cart can take items"() {
        when: "The cart takes an item"
        cart.putProductInto(aProduct(), new Quantity(5))

        then: "the shopping cart is not empty"
        !cart.isEmpty()
    }

    def "A shopping cart can take items of different products"() {
        when: "The cart takes different items"
        cart
            .putProductInto(aProduct(), new Quantity(5))
            .putProductInto(anotherProduct(), new Quantity(1))

        then: "the shopping cart is not empty"
        !cart.isEmpty()

        and: "The products are in the cart"
        cart.quantityOfProduct(sku).get().value == 5
        cart.quantityOfProduct(secondSku).get().value == 1
    }

    def "A shopping cart can list its contents"() {
        when: "The cart takes different items"
        cart
            .putProductInto(aProduct(), new Quantity(5))
            .putProductInto(anotherProduct(), new Quantity(1))

        then: "the shopping cart is not empty"
        List<Pair<Product, Quantity>> list = cart.content()

        and: "The products are in the cart"
        list.contains(new Pair(aProduct(), new Quantity(5)))
        list.contains(new Pair(anotherProduct(), new Quantity(1)))
    }

    def "A shopping cart can take items to a quantity of 10"() {
        when: "The cart takes an item"
        cart.putProductInto(aProduct(), new Quantity(5))
            .putProductInto(aProduct(), new Quantity(5))

        then: "the shopping cart is not empty"
        !cart.isEmpty()
    }

    def "A shopping cart sums the quantities of its products"() {
        when: "The cart takes an item"
        cart.putProductInto(aProduct(), new Quantity(5))
            .putProductInto(aProduct(), new Quantity(5))

        then: "the shopping cart has 10 products of the given sku"
        def optional = cart.quantityOfProduct(sku)
        optional.isPresent()
        optional.get().value == 10
    }

    def "An exceeding quantity of a products result in an exception and has no effect on the shopping cart"() {
        given: "The cart with 5 items of a product"
        cart.putProductInto(aProduct(), new Quantity(5))

        when: "We add 6 more items of the same product"
        cart.putProductInto(aProduct(), new Quantity(6))

        then: "an exception occurs"
        thrown(TooMuchItemsOfAProduct)

        and: "The shopping cart is not effected"
        cart.quantityOfProduct(sku).get().value == 5
    }

    def "A shopping cart can calculate the amount of its items"() {
        given: "The cart with 2 items of a product with a price of 2,99"
        cart.putProductInto(aProduct(new Price(2, 99)), new Quantity(2))

        when: "We add 3 more items of a product with a price of 3,49"
        cart.putProductInto(anotherProduct(new Price(3, 49)), new Quantity(3))

        then: "The shopping cart has an amount of 16,45 €"
        cart.amount() == new ShoppingCartAmount(16, 45)
    }

    def "An exceeding total amount over 300,00 result in an exception and has no effect on the shopping cart"() {
        given: "The cart with an amount of 299,99"
        cart.putProductInto(aProduct(new Price(100, 00)), new Quantity(2))
            .putProductInto(anotherProduct(new Price(99, 99)), new Quantity(1))

        when: "We add another product"
        cart.putProductInto(aProduct(new Price(100, 00)), new Quantity(1))

        then: "an exception is thrown"
        thrown(MaximumShoppingCardAmountExceededException)

        and: "The amount is still 299,99"
        cart.amount() == new ShoppingCartAmount(299,99)
    }

    def "A shopping cart can not have more than 50 different products"() {
        given: "A cart with 50 products"
        ShoppingCart cart = aCartWithFiftyProducts()

        when: "We add another product"
        cart.putProductInto(new Product(new SKU("51"), new Price(1, 0), new Name("Milch")), new Quantity(1))

        then: "It occurs in an exception"
        thrown(MaximumProductCountExceededException)
    }

    private ShoppingCart aCartWithFiftyProducts() {
        (1..50).each {
            it -> cart.putProductInto(new Product(new SKU("$it"), new Price(1, 0), new Name("Joghurt")), new Quantity(1))
        }
        return cart
    }

    private Product aProduct(Price price = new Price(10, 00)) {
        return new Product(sku, price, new Name("Schokolade"))
    }

    private Product anotherProduct(Price price = new Price(10, 00)) {
        return new Product(secondSku, price, new Name("Brot"))
    }

}
