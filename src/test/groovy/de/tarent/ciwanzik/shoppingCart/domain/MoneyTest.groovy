package de.tarent.ciwanzik.shoppingCart.domain

import spock.lang.Specification
import spock.lang.Unroll

@SuppressWarnings("GroovyResultOfObjectAllocationIgnored")
class MoneyTest extends Specification {

    def "A price is money"() {
        expect:
        new Price(50, 99) instanceof Money
    }

    @Unroll
    def "A price can not be negative"() {
        when: "negative price elements"
        new Price(euro, cent)

        then: "An exception occurs"
        thrown(IllegalArgumentException)

        where:
        euro | cent
        10   | -1
        -1   | 10
        -1   | -1
    }

    def "Cent can not exceed 99"() {
        when: "Cent of 100"
        new Price(10, 100)

        then: "No exception occurs"
        thrown(IllegalArgumentException)
    }

    def "A price can no exceed 120,00 €"() {
        when: "a zero price of 120,01 €"
        new Price(120, 1)

        then: "No exception occurs"
        thrown(TooHighPriceException)
    }

    def "A price can be zero"() {
        when: "a zero price"
        new Price(0, 0)

        then: "No exception occurs"
        noExceptionThrown()
    }

    def "A price can be 120,00"() {
        when: "a price of 120,00 €"
        new Price(120, 00)

        then: "No exception occurs"
        noExceptionThrown()
    }

    def "A price can be 0,01"() {
        when: "a price of 0,01 €"
        new Price(0, 1)

        then: "No exception occurs"
        noExceptionThrown()
    }

    @Unroll
    def "A price can be added to another"() {
        when: "We add two prices"
        def sum = firstPrice + secondPrice

        then: "The new price is as expected"
        sum.valueInCent == expectedValue

        where:
        firstPrice          | secondPrice       | expectedValue
        new Price(10, 99)   | new Price(2, 89)  | 1388
        new Price(0, 99)    | new Price(0, 99)  | 198
        new Price(111, 59)  | new Price(0, 41)  | 11200
    }

    def "A zero price added to another does not have any effect"() {
        given: "A price of 10,99"
        def price = new Price(10, 99)

        when: "We add a price of 0"
        def sum = price + new Price(0, 0)

        then: "The price is still 10,99"
        sum.valueInCent == 1099
        sum.euro == 10
        sum.cent == 99
    }

    def "Two prices can not exceed 120,00 €"() {
        given: "A price of 50,99"
        def price = new Price(50, 99)

        when: "We add a price of 69,02"
        price + new Price(69, 2)

        then: "an exception occurs"
        thrown(TooHighPriceException)
    }

    def "An amount is money"() {
        expect:
        new ShoppingCartAmount(50, 99) instanceof Money
    }

    def "Two amounts can exceed 120,00 €"() {
        given: "An amount of 50,99"
        def amount = new ShoppingCartAmount(50, 99)

        when: "We add an amount of 69,02"
        amount + new ShoppingCartAmount(69, 2)

        then: "an exception occurs"
        noExceptionThrown()
    }

    def "A single amount can exceed 120,00 €"() {
        when: "An amount of 120,01"
        new ShoppingCartAmount(120, 1)

        then: "an exception occurs"
        noExceptionThrown()
    }
}
